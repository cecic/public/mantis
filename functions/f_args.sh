#
# Content : useful finctions to manage argument list
#

# get the last argument
function args_last() {
  echo ${@: -1};
}

# get the argument list except the last argument
function args_except_last() { 
  echo ${@:1:$#-1}; 
}
