#
# Content: useful functions to manage command, stdout and stderr
#
# Author: Pierre GIRARD <pierre.girard@univ-grenoble-alpes.f>
#


# Function that catches from a command the stdout and the stderr in variables
# STDOUT is contained in $stdout and STDERR is contained in $stderr afterwards, if any
function run_catch_stdout_and_stderr() {
  local cmd=$*;
  {
    stdout=$(${cmd} 2>/dev/fd/${myfd});
    status=$?;
    stderr=$(cat<&${myfd});
  } {myfd}<<EOF
EOF
  return $status;
}

# if dry run mode is enabled just display the command, run it otherwise
# By default dry run is disabled
_RUN_DRYRUN=0

function run_is_dryrun() {
  [ ${_RUN_DRYRUN} -eq 1 ]
}

function run_dryrun_activate() {
  _RUN_DRYRUN=1
}

function run_dryrun_deactivate() {
  _RUN_DRYRUN=0
}


function run_dryrun() {
  local command=$*

  # If no command, just warn
  [ -n "${command}" ] || {
    echo "[WARNING] no command given to run_dryrun function. Skip it" 1>&2
    return 0;
  }

  case ${_RUN_DRYRUN} in
    0)  source <<PROG /dev/fd/0;;
${command}
PROG
    1)  echo "${command} || { ";
        echo "  echo \"[ERROR] command [${command}] failed [ecode=\$?]\" 1>&2;"
        echo "  exit 1;"
        echo "}";;
    *)  echo "[ERROR] run_dryrun function: bad value of _RUN_DRYRUN variable [current=${_RUN_DRYRUN}], must be 0 or 1" 1>&2;
        return 1;;
  esac
}

# If dryrun, then add a comment character (#) at the begining of the line if the comment
# is not empty, otherwise create a blank line
function run_comment() {
  local comment="$*"

  case ${_RUN_DRYRUN} in
    0)  echo "${comment}";;
    1)  [ -n "${comment}" ] && echo "# ${comment}" || echo;;
    *)  echo "[ERROR] run_comment function: bad value of _RUN_DRYRUN variable [current=${_RUN_DRYRUN}], must be 0 or 1" 1>&2;
        return 1;;
  esac
}

function run_echo() {
  local message=$*

  case ${_RUN_DRYRUN} in
    0)  echo "${message}";;
    1)  echo "echo \"${message}\"";;
    *)  echo "[ERROR] run_echo function: bad value of _RUN_DRYRUN variable [current=${_RUN_DRYRUN}], must be 0 or 1" 1>&2;
        return 1;;
  esac
}
