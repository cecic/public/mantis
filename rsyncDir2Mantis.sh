#!/bin/bash

CMD_NAME=$(basename $0)
CMD_DIR=$(dirname $(readlink -f $0))

ETC_DIR=${CMD_DIR}/etc
FUNC_DIR=${CMD_DIR}/functions

# Load IRODS environment
source /applis/site/nix.sh
[ $? -ne 0 ] && exit 1;

# Check some important environment variable
[ -n "${CLUSTER_NAME}" ] || {
  echo "[WARNING] variable CLUSTER_NAME should be set." 1>&2
}

# [ -n "${}" ] || 

CLUSTER=${CLUSTER_NAME}

#
# Useful function
# 

# Management of an argument list
source ${FUNC_DIR}/f_args.sh || exit 1;

# Management of commands run or dryrun
source ${FUNC_DIR}/f_run.sh


# usage <exit_code>
function usage() {
  local ecode=$1

  echo "Usages:"
  echo " ${CMD_NAME} -h"
  echo " ${CMD_NAME} [-n] [-i] [-Y] [-D] <dir> <mantis_dir>";
  echo " ${CMD_NAME} [-n] [-i] [-Y] [-D] <dir> [<dir> ...] <mantis_dir>";
  echo
  echo "Description:"
  echo " If only one local directory <dir> is given, its content"
  echo " will be synchronized with the Mantis collection <mantis_dir>."
  echo " If more than one local directory <dir> is given, the content"
  echo " of each directory '<dir>' will be synchronized with"
  echo " sub-collection '<dir>' within the collection <mantis_dir>."
  echo
  echo "Symbolic link case:"
  echo " By default, the symbolic links are ignored. See option -D"
  echo " for more information about this topic."
  echo " Due to a bug, synchronization will fail if there is any broken link"
  echo " (symbolic link to non-existent file/directory). Before rsyncing a"
  echo " directory, this script will check if there is any broken link into"
  echo " this directory. In such a case, you will be asked if you want to remove"
  echo " all the broken link in your local directory. If 'yes', all the broken"
  echo " will be removed before synchronizing. If 'no', the synchronization of"
  echo " the directory is aborted. If 'quit', no more synchronizations are done"
  echo " and the program stops. The -Y option silently answers 'yes' to every"
  echo " broken link deletion request."
  echo
  echo "Arguments:"
  echo " <dir> [<dir> ...]: list of local directories to be synchronized."
  echo " <mantis_dir>: remote directory (irods collection) in Mantis iRODS service."
  echo
  echo "Options:"
  echo " -h: help displaying this usage."
  echo " -n: dry run, don't play the commands but displays instead their sequence"
  echo "     on the standard output so that you can redirect it in a file to run it"
  echo "     later."
  echo " -i: by default this program stores the data on Mantis resource 'backup' which"
  echo "     make automatically 2 replicas of the data on 2 different datacentres."
  echo "     This is safer for precious data to be archived but slower and that"
  echo "     reduces your quota by 2. By using option '-n' you will switch the storage"
  echo "     to Mantis resource 'imag' faster but without replica, so less safe."
  echo " -Y: silently answers 'yes' to every broken link deletion request"
  echo " -D: dereference and follow the symbolic links and copy the"
  echo "     files they point to. Symbolic links are never copied "
  echo "     and are ignored without this option."


  exit $ecode;
}

YES=0
DEREF=0
BKUP=1
while getopts "hc:DnY" Option
do
  case $Option in
    h) usage 0;;
    D) DEREF=1;;
    n) run_dryrun_activate;;
    Y) YES=1;;
    *) echo "Unexpected option" 1>&2; usage 2;;
  esac
done

# Skip options arguments so that it points to not optional arguments
shift $(($OPTIND - 1))

# Variable used to determine if we are synchronising only one dir or not
# <dir> <mantis_dir> => YES
# <dir> <dir>... <mantis_dir> => NO
ONE_DIR_MODE="NO"

ARGS_COUNTER=$#
case ${ARGS_COUNTER} in
  0|1)  echo "[ERROR] not enough arguments, at least 2 expected" 1>&2
        echo; usage 1;;
  2)    # One directory synchronization mode
        ONE_DIR_MODE="YES";;
esac

# Take the list of dirs from arguments
DIR_LIST="$*"

# The last one is the MANTIS iRODS collection destination
MANTIS_COLLECTION=$(args_last ${DIR_LIST})

# The others are the local directories
LOCAL_DIRS=$(args_except_last ${DIR_LIST})

# If backup is requested (not "-i"), set the resource option for iRODS command
[ ${BKUP} -eq 1 ] && RESOURCE_OPT="-R backup";

# If dereference is asked, don't use the --link option of irsync command
IRSYNC_OPT=""
[ ${DEREF} -eq 1 ] || IRSYNC_OPT="--link";


# We check if each local dir exists
for d in $(echo ${LOCAL_DIRS}); do 
  [ -d "$d" ] ||
    {
    echo "[ERROR] [$d] is not a directory" 1>&2
    exit 1
    }
done

#
# Main 
#

# If dryrun activated, we create a script header and 
# load required environment
$(run_is_dryrun) && {
  echo "#!/bin/bash"
  echo "# Date: $(date)"
  echo 
  echo "# Load irods environment"
  echo "source /applis/site/nix.sh"
}

skipped=""
for d in $(echo ${LOCAL_DIRS}); do

  # We work with absolute path to avoid '.'
  abs_d=$(readlink -f $d)
  dname=$(basename ${abs_d})

  # Depending on the mode, set the targetted mantis directory
  mantis_dir=${MANTIS_COLLECTION}
  [ "${ONE_DIR_MODE}" = "NO" ] && mantis_dir=${mantis_dir}/${dname};

  echo
  echo "##################"
  echo "#### Synchronising [${abs_d}] with [i:${mantis_dir}]"
  echo "##################"

  # check if we have broken links, put stdout in $stdout and $stderr in $err and exit code in ecode
  run_catch_stdout_and_stderr find -P ${abs_d} -xtype l -ls
  rcode=$?

  # IRODS could not accept cyclic symlinks
  [ $rcode -ne 0 ] && {
    echo "[ERROR] it seems that at least a symlink would be problematic (ex.: loop) for iRODS in [${d}]. Skipping." 1>&2
    echo "Below the problematic case(s):" 1>&2
    # echo "${stderr}" | sed -e 's/^/ => /' -e "s/find: '//" -e "s/': Too many levels of symbolic links//" 1>&2
    echo "${stderr}" | sed -e 's/^/ => /' 1>&2
    [ -n "${skipped}" ] && skipped="${skipped} "${d} || skipped="${d}"
    continue
  }

  # Check if we have potential problems
  dangling_symlinks=$(echo "${stdout}" | sed -r -e '/^\s*$/d'| wc -l)
  [ ${dangling_symlinks} -gt 0 ] && {
    echo "[WARNING] we detected [${dangling_symlinks}] broken symlink(s) in [${abs_d}]" 1>&2
    answer=""
    while [ 1 ]; do
      answer=YES
      [ ${YES} -ne 1 ] && {
        read -p "Do you want to remove each broken symlink in [${abs_d}] (yes|no|quit|show): " answer
        # upperize answer
        answer=$(echo "${answer}" | tr "[a-z]" "[A-Z]")
      }
      case ${answer} in
        NO)   echo "[WARNING] skipping synchronization of [${d}]" 1>&2;
              continue 2;;
        QUIT) echo "[WARNING] Stopping all the remaining synchronization." 1>&2;
              break 2;;
        SHOW) echo "${stdout}" | sed 's/^/ => /' 1>&2; continue;;
        YES)  run_comment "Deletion of [${dangling_symlinks}] dangling symlink(s) in [${abs_d}]";
              run_dryrun find -P ${abs_d} -xtype l -delete
              [ $? -ne 0 ] && {
                echo "[ERROR] symlink deletion failed. Skipping [${d}]" 1>&2;
                [ -n "${skipped}" ] && skipped="${skipped} "${d} || skipped="${d}"
                continue 2;
              }
              break;;
        *) echo "Please, answer: yes, no, quit or show" 1>&2;;
      esac
    done
  }

  # Finally irsync the directory in the collection

  # If dereference is asked, don't use the --link option of irsync command
  IRSYNC_OPT=""
  [ ${DEREF} -eq 1 ] || IRSYNC_OPT="--link";
  
  run_dryrun "irsync -r ${RESOURCE_OPT} ${IRSYNC_OPT} ${abs_d} i:${mantis_dir}"

done

