#!/bin/bash

CMD_NAME=$(basename $0)
CMD_DIR=$(dirname $(readlink -f $0))

ETC_DIR=${CMD_DIR}/etc
FUNC_DIR=${CMD_DIR}/functions

# Load IRODS environment
source /applis/site/nix.sh
[ $? -ne 0 ] && {
  echo "[ERROR] cannot source [/applis/site/nix.sh]" 1>&2
  exit 1;
}

# Check some important environment variable
[ -n "${CLUSTER_NAME}" ] || {
  echo "[WARNING] variable CLUSTER_NAME should be set." 1>&2
}

[ -n "${SHARED_SCRATCH_DIR}" ] || {
   echo "[WARNING] variable SHARED_SCRATCH_DIR should be set." 1>&2
}

CLUSTER=${CLUSTER_NAME}
DFT_TMPDIR=${SHARED_SCRATCH_DIR}/$USER/mantis_tmp
DFT_MANTIS_RESOURCE=backup

#
# Useful functions
#

# Management of an argument list
source ${FUNC_DIR}/f_args.sh || {
  echo "[ERROR] cannot source [${FUNC_DIR}/f_args.sh]" 1>&2
  exit 1;
}

# Management of commands run or dryrun
source ${FUNC_DIR}/f_run.sh || {
  echo "[ERROR] cannot source [${FUNC_DIR}/f_run.sh]" 1>&2
  exit 1;
}

function addDir2Tar()
  {
  local dir=$1
  local tar=$2

  rootdir=$(dirname $(readlink -f ${dir}))
  reldir=$(basename $(readlink -f ${dir})) 


  run_comment
  run_comment "Go to directory [${rootdir}]"
  run_dryrun cd ${rootdir}

  run_comment
  run_comment "Add directory [${dir}] to tar archive [${tar}]"

  case $DEREF in
    0)  # Add all except the links
        run_dryrun "find $reldir -not -type l -exec tar -uf ${tar} --no-recursion '{}' \;";
        rcode=$?;;
    1)  # Add all and deference the links
        run_dryrun "tar --dereference -uf ${tar} $reldir";
        rcode=$?;;
    *) rcode=1;;
  esac

  run_comment
  run_comment "Go back to working directory"
  run_dryrun "cd -"

  return $rcode
  }

function ibunTar2Mantis()
  {
  local tar=$1
  local dest=$2

  run_comment
  run_comment "Check if tar archive [${tar}] exists"
  run_dryrun [ -f "${tar}" ] 
  [ $? -ne 0 ] &&
    {
    echo "[ERROR] Tar file [${tar}] not found" 1>&2
    return 1
    }

  # Check integrity
  run_comment
  run_comment "Check integrity of tar archive [${tar}] exists"
  run_dryrun tar tf ${tar} &>/dev/null
  [ $? -ne 0 ] &&
    {
    echo "[ERROR] Tar file [${tar}] not valid" 1>&2
    return 2
    }
  run_echo "Tar archive [${tar}] is ok"

  run_comment
  run_comment "Check if Mantis collection [${dest}] exists"
  ils ${dest} &>/dev/null && 
    {
    run_comment "Yes, Mantis collection [${dest}] already exists."
    } || 
    {
    run_comment "No, Mantis collection [${dest}] does not exist. Create it"
    run_comment
    run_comment "Create Mantis collection [${dest}]"
    run_dryrun imkdir ${dest}
    [ $? -ne 0 ] &&
      {
      echo "[INFO] Something went wrong with the creation of IRODS collection [${dest}]" 1>&2
      return 3
      }
    run_echo "Mantis collection [${dest}] has just been successfully created"
    }
  
  # iput ${FORCE_OPT} -Dtar ${tar} ${dest}
  run_comment
  run_comment "Transfer tar archive [${tar}] to Mantis collection [${dest}]"
  run_dryrun iput ${RESOURCE_OPT} -Dtar ${tar} ${dest}
  [ $? -ne 0 ] && return 4;
  run_echo "Tar archive [${tar}] has been successfully copied to iRODS collection [${dest}]"

  tarname=$(basename ${tar})
  # ibun ${FORCE_OPT} -x ${dest}/${tarname} ${dest}
  run_comment
  run_comment "Untar [${dest}/${tarname}] in Mantis"
  run_dryrun ibun ${RESOURCE_OPT} -x ${dest}/${tarname} ${dest}
  run_echo "Tar archive [${tar}] has been extracted into iRODS collection [${dest}]"
  }

# usage <exit_code>
function usage() {
  local ecode=$1

  echo "Usages:"
  echo " ${CMD_NAME} -h"
#  echo " ${CMD_NAME} [-f] [-t <tmpdir>] [-c <collection>] [-D] <dir> [<dir> ...]";
  echo " ${CMD_NAME} [-n] [-i] [-t <tmpdir>] [-D] <dir> [<dir> ...] <mantis_dir>";
  echo
  echo "Description:"
  echo " Copy the content of the directories in arguments to"
  echo " Mantis IRODS service within the iRODS collection <mantis_dir>."
  echo " By default, the symbolic links are ignored. See option -D"
  echo " for more information about this topic."
  echo
  echo "Arguments:"
  echo " <dir> [<dir> ...]: list of local directories to be copied."
  echo " <mantis_dir>: the destinatory Mantis collection of the copy."
  echo
  echo "Options:"
  echo " -h: help displaying this usage."
#  echo " -f: Enforce the update if the directory already exits within"
#  echo "     the collection destination. WARNING: does not work with ibun."
  echo " -n: dry run, don't play the commands but displays instead their sequence"
  echo "     on the standard output so that you can redirect it in a file to run it"
  echo "     later."
  echo " -i: by default this program stores the data on Mantis resource 'backup' which"
  echo "     make automatically 2 replicas of the data on 2 different datacentres."
  echo "     This is safer for precious data to be archived but slower and that"
  echo "     reduces your quota by 2. By using option '-n' you will switch the storage"
  echo "     to Mantis resource 'imag' faster but without replica, so less safe."
  echo " -t <tmpdir>: this script uses a temporary directory to create"
  echo "    a local tar archive. By default this directroy is"
  echo "    [${DFT_TMPDIR}]."
  echo "    Make sure there is enough space available in this directory"
  echo "    according to the amount of data to be archived."
  echo " -D: dereference and follow the symbolic links and copy the"
  echo "     files they point to. Symbolic links are never copied "
  echo "     and are ignored without this option."

  exit $ecode;
}

#
# Command line parsing
#

FORCE=0
DEREF=0
BKUP=1
#while getopts "hfc:t:D" Option
while getopts "hnit:D" Option
do
  case $Option in
    h) usage 0;;
#    f) FORCE=1;;
    i) BKUP=0;;
    n) run_dryrun_activate;;
    t) TMPDIR=$OPTARG;;
    D) DEREF=1;;
    *) echo "Unexpected option" 1>&2; usage 2;;
  esac
done

# Skip options arguments so that it points to not optional arguments
shift $(($OPTIND - 1))

ARGS_COUNTER=$#
case ${ARGS_COUNTER} in
  0|1)  echo "[ERROR] not enough arguments, at least 2 expected" 1>&2
        echo; usage 1;;
  2)    # One directory synchronization mode
        ONE_DIR_MODE="YES";;
esac

# Take the list of dirs from arguments
DIR_LIST="$*"

# The last one is the MANTIS iRODS collection destination
MANTIS_COLLECTION=$(args_last ${DIR_LIST})

# The others are the local directories
LOCAL_DIRS=$(args_except_last ${DIR_LIST})


[ ${BKUP} -eq 1 ] && RESOURCE_OPT="-R backup";

# If necessary, use the default temporary directory
[ -n "${TMPDIR}" ] || TMPDIR=${DFT_TMPDIR};
[ -d "${TMPDIR}" ] || mkdir -p ${TMPDIR} ||
  {
  echo "[ERROR] Temporary directory [${TMPDIR}] cannot be created" 1>&2
  exit 1
  }

[ $FORCE -eq 1 ] && 
  {
  echo "[WARNING] The force option does not work well with ibun" 1>&2
  FORCE_OPT="-f";
  }


for d in $(echo ${LOCAL_DIRS}); do 
  [ -d "$d" ] ||
    {
    echo "[ERROR] [$d] is not a directory" 1>&2
    exit 1
    }
done

#
# Main 
#
tarname=${USER}_mantis.tar

# If dryrun activated, we create a script header and
# load required environment
$(run_is_dryrun) && {
  echo "#!/bin/bash"
  echo "# Date: $(date)"
  echo
  echo "# Load irods environment"
  echo "source /applis/site/nix.sh"
  echo
}


[ -f ${TMPDIR}/${tarname} ] && rm ${TMPDIR}/${tarname};

for d in $(echo ${LOCAL_DIRS}); do
  addDir2Tar ${d} ${TMPDIR}/${tarname}
  [ $? -ne 0 ] && 
    {
    echo "[ERROR] addDir2Tar [${d}=>${TMPDIR}/${tarname}] failed" 1>&2
    exit 3
    }
done

ibunTar2Mantis ${TMPDIR}/${tarname} ${MANTIS_COLLECTION}
[ $? -ne 0 ] && exit 4;

run_comment
run_comment "Finally, remove the tar archive [${MANTIS_COLLECTION}/${tarname}] from Mantis"
run_dryrun irm ${MANTIS_COLLECTION}/${tarname}
run_echo "Tar archive [${tarname}] has been removed from Mantis collection [${MANTIS_COLLECTION}]"

run_comment
run_dryrun ils -lr ${MANTIS_COLLECTION}

